import { NgModule } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';

import { LoginModule } from './login/login.module';

import { AppComponent } from './app.component';

@NgModule({
	declarations: [
		AppComponent,
	],
	bootstrap: [AppComponent],
	imports: [
		NativeScriptModule,
		LoginModule
	]
})
export class AppModule {}
