import { NgModule } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';


// component
import { LoginComponent } from './login.component';


@NgModule({
	declarations: [
		LoginComponent
	],
	imports: [
		NativeScriptModule
	],
	exports: [
		LoginComponent
	]
})


export class LoginModule {}